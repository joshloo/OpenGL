# Author: Josh Loo
# Found bug? Created an improved version? 
# Let me know. joshlootunglun@gmail.com

from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *

leftFirst = 1;
blendfunc = 0;

def initFun():
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glShadeModel(GL_FLAT);
    glClearColor(0.0, 0.0, 0.0, 0.0);
    
def drawLeftTriangle():
    glBegin(GL_TRIANGLES)
    glColor4f(1.0, 1.0, 0.0, 0.75);
    glVertex3f(0.1, 0.9, 0.0); 
    glVertex3f(0.1, 0.1, 0.0); 
    glVertex3f(0.7, 0.5, 0.0); 
    glVertex3f(0.7, 0.5, 0.0); 
    glEnd();

def drawRightTriangle():
    glBegin(GL_TRIANGLES)
    glColor4f(0.0, 1.0, 1.0, 0.75);
    glVertex3f(0.9, 0.9, 0.0); 
    glVertex3f(0.3, 0.5, 0.0); 
    glVertex3f(0.9, 0.1, 0.0); 
    glEnd();    
    
def displayFun():
    glClear(GL_COLOR_BUFFER_BIT);

    if leftFirst == 1:
        drawLeftTriangle();
        drawRightTriangle();
    elif leftFirst == 0:
        drawRightTriangle();
        drawLeftTriangle();
        
    if blendfunc == 0:
        # works
        print "Blend func: GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA"
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    elif blendfunc == 1:
        # works, constantly yellow
        print "Blend func: GL_SRC_ALPHA, GL_ONE"
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);  
    elif blendfunc == 2:
        # works, similar to bf 0 
        print "Blend func: GL_SRC_ALPHA_SATURATE, GL_ONE"
        glBlendFunc(GL_SRC_ALPHA_SATURATE, GL_ONE);  
    elif blendfunc == 3:
        # works, totally opaque
        print "Blend func: GL_ONE, GL_ZERO"
        glBlendFunc(GL_ONE, GL_ZERO);        
    elif blendfunc == 4:
        # totally black
        print "Blend func: GL_ZERO, GL_ONE"
        glBlendColor(1.0, 1.0, 1.0, 0.5) 
        glBlendFunc(GL_ZERO, GL_ONE);        
    elif blendfunc == 5:
        # totally black
        print "Blend func: GL_DST_COLOR, GL_ONE"
        glBlendColor(1.0, 1.0, 1.0, 0.5) 
        glBlendFunc(GL_DST_COLOR, GL_ONE);        
    elif blendfunc == 6:
        # totally black
        print "Blend func: GL_DST_COLOR, GL_ZERO"
        glBlendColor(1.0, 1.0, 1.0, 0.5) 
        glBlendFunc(GL_DST_COLOR, GL_ZERO);        
    elif blendfunc == 7:
        # works, followed constant alpha
        print "Blend func: GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA"
        glBlendColor(1.0, 1.0, 1.0, 0.5) 
        glBlendFunc(GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);        
    elif blendfunc == 8:
        # black 
        print "Blend func: GL_SRC1_COLOR, GL_ONE_MINUS_SRC1_COLOR"
        glBlendColor(1.0, 1.0, 1.0, 0.5) 
        glBlendFunc(GL_SRC1_COLOR, GL_ONE_MINUS_SRC1_COLOR);        
    glFlush();

def reshape(w, h):
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w <= h):
        gluOrtho2D (0.0, 1.0, 0.0, 1.0*float(h)/float(w));
    else:
        gluOrtho2D (0.0, 1.0*float(w)/float(h), 0.0, 1.0);

def keyboard(*args):
    global leftFirst, blendfunc
    key = args[0]
    if key == 't' or key == 'T':
        leftFirst = 1;
        glutPostRedisplay();
    elif key == 'y' or key == 'Y':
        leftFirst = 0;
        glutPostRedisplay();
    elif key == '0':
        print "Combo 0"
        blendfunc = 0;
        glutPostRedisplay();
    elif key == '1':
        print "Combo 1"
        blendfunc = 1;
        glutPostRedisplay();
    elif key == '2':
        print "Combo 2"
        blendfunc = 2;
        glutPostRedisplay();
    elif key == '3':
        print "Combo 3"
        blendfunc = 3;
        glutPostRedisplay();
    elif key == '4':
        print "Combo 4"
        blendfunc = 4;
        glutPostRedisplay();
    elif key == '5':
        print "Combo 5"
        blendfunc = 5;
        glutPostRedisplay();
    elif key == '6':
        print "Combo 6"
        blendfunc = 6;
        glutPostRedisplay();
    elif key == '7':
        print "Combo 7"
        blendfunc = 7;
        glutPostRedisplay();
    elif key == '8':
        print "Combo 8"
        blendfunc = 8;
        glutPostRedisplay();
   
    elif key == 'Z' or key == 'z':
        exit(0);
             
    
if __name__ == '__main__':
    glutInit()
    glutInitWindowSize(640,480)
    glutCreateWindow("Draw Triangles")
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA)
    initFun()
    glutReshapeFunc(reshape)
    glutKeyboardFunc(keyboard)
    glutDisplayFunc(displayFun)
    glutMainLoop()